import org.junit.jupiter.api.Test;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

public class ReferenceDemoTest {

    @Test
    public void studiedDisciplinesTest() throws ReferenceException, ReferenceDemoException {
        List<TableString> list = new ArrayList<>();
        list.add(new TableString("Algebra", 96, Mark.GOOD));
        list.add(new TableString("Physical Culture", 40, Mark.OFFSET));
        list.add(new TableString("Calculus", 123, Mark.SATISFACTORILY));
        list.add(new TableString("History", 56, Mark.EXCELLENT));

        Reference reference = new Reference("Ivan Ivanovich Ivanov", "Omsu",
                "Computer Science","Computer security", "01.09.2012 year",
                "4.06.2016", list);


        List<String> res = new ArrayList<>(
                Arrays.asList("Algebra", "Physical Culture", "Calculus", "History"));
        List<String> actRes = ReferenceDemo.studiedDisciplines(reference);
        assertIterableEquals(res, actRes);
    }

    @Test
    public void allSubjectHoursTest() throws ReferenceException, ReferenceDemoException {
        List<TableString> list = new ArrayList<>();
        list.add(new TableString("Algebra", 96, Mark.GOOD));
        list.add(new TableString("Physical Culture", 40, Mark.OFFSET));
        list.add(new TableString("Calculus", 123, Mark.SATISFACTORILY));
        list.add(new TableString("History", 56, Mark.EXCELLENT));

        Reference reference = new Reference("Ivan Ivanovich Ivanov", "Omsu",
                "Computer Science","Computer security", "01.09.2012 year",
                "4.06.2016", list);


        assertEquals(315, ReferenceDemo.allSubjectsHours(reference));
    }

    @Test
    public void averageRatingTest() throws ReferenceException, ReferenceDemoException {
        List<TableString> list = new ArrayList<>();
        list.add(new TableString("Algebra", 96, Mark.GOOD));
        list.add(new TableString("Physical Culture", 40, Mark.GOOD));
        list.add(new TableString("Calculus", 123, Mark.SATISFACTORILY));
        list.add(new TableString("History", 56, Mark.EXCELLENT));
        list.add(new TableString("Geometry", 24, Mark.SATISFACTORILY));

        Reference reference = new Reference("Ivan Ivanovich Ivanov", "Omsu",
                "Computer Science","Computer security", "01.09.2012 year",
                "4.06.2016", list);


        assertEquals(3.8, ReferenceDemo.averageRating(reference), 0.0001);
    }

    @Test
    public void subjectsToMarkTest() throws ReferenceException, ReferenceDemoException {
        List<TableString> list = new ArrayList<>();
        list.add(new TableString("Algebra", 96, Mark.GOOD));
        list.add(new TableString("Physical Culture", 40, Mark.GOOD));
        list.add(new TableString("Calculus", 123, Mark.SATISFACTORILY));
        list.add(new TableString("History", 56, Mark.EXCELLENT));
        list.add(new TableString("Geometry", 24, Mark.SATISFACTORILY));

        Reference reference = new Reference("Ivan Ivanovich Ivanov", "Omsu",
                "Computer Science","Computer security", "01.09.2012 year",
                "4.06.2016", list);

        Map<String, Mark> expRes = new HashMap<>();
        expRes.put("Algebra", Mark.GOOD);
        expRes.put("Physical Culture", Mark.GOOD);
        expRes.put("Calculus", Mark.SATISFACTORILY);
        expRes.put("History", Mark.EXCELLENT);
        expRes.put("Geometry", Mark.SATISFACTORILY);

        assertEquals(expRes, ReferenceDemo.subjectsToMark(reference));
    }

    @Test
    public void markToSubjectsTest() throws ReferenceException, ReferenceDemoException {
        List<TableString> list = new ArrayList<>();
        list.add(new TableString("Algebra", 96, Mark.GOOD));
        list.add(new TableString("Physical Culture", 40, Mark.GOOD));
        list.add(new TableString("History", 56, Mark.EXCELLENT));
        list.add(new TableString("Calculus", 123, Mark.SATISFACTORILY));
        list.add(new TableString("Philosophy", 63, Mark.EXCELLENT));

        Reference reference = new Reference("Ivan Ivanovich Ivanov", "Omsu",
                "Computer Science","Computer security", "01.09.2012 year",
                "4.06.2016", list);

        Map<Mark, List<String>> expRes = new HashMap<>();
        expRes.put(Mark.GOOD, new ArrayList<>(Arrays.asList("Algebra", "Physical Culture")));
        expRes.put(Mark.EXCELLENT, Arrays.asList("History", "Philosophy"));
        expRes.put(Mark.SATISFACTORILY, new ArrayList<>(Collections.singletonList("Calculus")));

        assertEquals(expRes, ReferenceDemo.markToSubjects(reference));
    }

}
