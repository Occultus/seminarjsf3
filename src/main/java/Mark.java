public enum Mark {

    EXCELLENT("отлично"),
    GOOD("хорошо"),
    SATISFACTORILY("удовлетворительно"),
    UNSATISFACTORILY("неудовлетворительно"),
    OFFSET("зачтено"),
    NEGLECT("не зачтено");
    private String mark;
    Mark(String mark){
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }

}
