public class TableString {
    private String subject;
    private int subjectHours;
    private Mark mark;

    public TableString(String subject, int subjectHours, Mark mark) {
        this.subject = subject;
        this.subjectHours = subjectHours;
        this.mark = mark;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getSubjectHours() {
        return subjectHours;
    }

    public void setSubjectHours(int subjectHours) {
        this.subjectHours = subjectHours;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }
}
