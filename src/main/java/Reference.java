import java.util.List;
import java.util.Objects;

public class Reference{
    private String fio;
    private String university;
    private String faculty;
    private String specialty;
    private String startDate, endDate;
    private List<TableString> tablePart;

    public Reference(String fio, String university, String faculty, String specialty,
                     String startDate, String endDate, List<TableString> tablePart) throws ReferenceException {
        if(fio == null || fio.isEmpty()){
            throw new ReferenceException("Empty/null pointer fio");
        }
        else if(university == null || university.isEmpty()){
            throw new ReferenceException("Empty/null pointer university");
        }
        else if(faculty == null || faculty.isEmpty()){
            throw new ReferenceException("Empty/null pointer faculty");
        }
        else if(specialty == null || specialty.isEmpty()){
            throw new ReferenceException("Empty/null pointer specialty");
        }
        else if(startDate == null || startDate.isEmpty()){
            throw new ReferenceException("Empty/null pointer startDate");
        }
        else if(endDate == null || endDate.isEmpty()){
            throw new ReferenceException("Empty/null pointer endDate");
        }
        this.fio = fio;
        this.university = university;
        this.faculty = faculty;
        this.specialty = specialty;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tablePart = tablePart;
    }

    public void setFio(String fio) throws ReferenceException {
        if(fio == null || fio.isEmpty()){
            throw new ReferenceException("Empty/null pointer fio");
        }
        this.fio = fio;
    }

    public String getFio() {
        return fio;
    }

    public void setUniversity(String university) throws ReferenceException {
        if(university == null || university.isEmpty()){
            throw new ReferenceException("Empty/null pointer university");
        }
        this.university = university;
    }

    public String getUniversity() {
        return university;
    }

    public void setFaculty(String faculty) throws ReferenceException {
        if(faculty == null || faculty.isEmpty()){
            throw new ReferenceException("Empty/null pointer faculty");
        }
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setSpecialty(String specialty) throws ReferenceException {
        if(specialty == null || specialty.isEmpty()){
            throw new ReferenceException("Empty/null pointer specialty");
        }
        this.specialty = specialty;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setStartDate(String startDate) throws ReferenceException {
        if(startDate == null || startDate.isEmpty()){
            throw new ReferenceException("Empty/null pointer startDate");
        }
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) throws ReferenceException {
        if(endDate == null || endDate.isEmpty()){
            throw new ReferenceException("Empty/null pointer endDate");
        }
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setTablePart(List<TableString> tablePart) {
        this.tablePart = tablePart;
    }

    public List<TableString> getTablePart() {
        return tablePart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reference reference = (Reference) o;
        return Objects.equals(fio, reference.fio) &&
                Objects.equals(university, reference.university) &&
                Objects.equals(faculty, reference.faculty) &&
                Objects.equals(specialty, reference.specialty) &&
                Objects.equals(startDate, reference.startDate) &&
                Objects.equals(endDate, reference.endDate) &&
                Objects.equals(tablePart, reference.tablePart);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fio, university, faculty, specialty, startDate, endDate, tablePart);
    }
}
