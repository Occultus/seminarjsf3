public class ReferenceException extends Exception {
    public ReferenceException(){
        super();
    }
    public ReferenceException(String errorText){
        super(errorText);
    }
    public ReferenceException(Exception exception){
        super(exception);
    }
    public ReferenceException(String errorText, Exception exception){
        super(errorText, exception);
    }
}
