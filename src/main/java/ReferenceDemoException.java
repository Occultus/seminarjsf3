public class ReferenceDemoException extends Exception{
    public ReferenceDemoException(String error){
        super(error);
    }
    public ReferenceDemoException(Exception exception){
        super(exception);
    }
    public ReferenceDemoException(){
        super();
    }
    public ReferenceDemoException(String error, Exception exception){
        super(error, exception);
    }
}
