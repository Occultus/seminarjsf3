import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReferenceDemo {
    public static @NotNull
    List<String> studiedDisciplines(Reference reference) throws ReferenceDemoException {
        if(reference == null){
            throw new ReferenceDemoException("reference if Null pointer");
        }
        List<String> res = new ArrayList<>(reference.getTablePart().size());
        for(TableString temp: reference.getTablePart()){
            res.add(temp.getSubject());
        }
        return res;
    }

    public static int allSubjectsHours(Reference reference) throws ReferenceDemoException {
        if(reference == null){
            throw new ReferenceDemoException("reference if Null pointer");
        }
        int res = 0;
        for(TableString temp: reference.getTablePart()){
            res += temp.getSubjectHours();
        }
        return res;
    }

    public static float averageRating(Reference reference) throws ReferenceDemoException {
        if(reference == null){
            throw new ReferenceDemoException("reference if Null pointer");
        }
        float res = 0;
        int countOfMarks = 0;
        for(TableString temp: reference.getTablePart()){
            Mark tempMark = temp.getMark();
            if(tempMark == Mark.EXCELLENT){
                res += 5;
                countOfMarks++;
            }
            else if(tempMark == Mark.GOOD){
                res += 4;
                countOfMarks++;
            }
            else if(tempMark == Mark.SATISFACTORILY){
                res += 3;
                countOfMarks++;
            }
        }
        if(countOfMarks > 0){
            return res/countOfMarks;
        }
        return res;
    }

    public static @NotNull
    Map<String, Mark> subjectsToMark(Reference reference) throws ReferenceDemoException {
        if(reference == null){
            throw new ReferenceDemoException("reference if Null pointer");
        }
        Map<String, Mark> res = new HashMap<>();
        for(TableString temp: reference.getTablePart()){
            res.put(temp.getSubject(), temp.getMark());
        }
        return res;
    }

    public static @NotNull
    Map<Mark, List<String>> markToSubjects(Reference reference) throws ReferenceDemoException {
        if(reference == null){
            throw new ReferenceDemoException("reference if Null pointer");
        }
        Map<Mark, List<String>> res = new HashMap<>();
        for(TableString temp: reference.getTablePart()){
            Mark tempMark = temp.getMark();
            if(!res.containsKey(tempMark)){
                res.put(tempMark, new ArrayList<>());
            }
            res.get(tempMark).add(temp.getSubject());
        }
        return res;
    }

}
